import React from 'react'
import Web from '../images/contactV.jpg'; 
 const Contact = () => {
    return (
        <>
 <section id="contact_one">
  <div className="container-fluid ">
  <div className="row">
    <div className="col-10 mx-auto">
    <div className="row">
       <div className="col-md-6  pt-lg-0 order-2 order-lg-1">
        
    <form id="contact">
    <h3><span className="brand">Venom</span> Contact Form</h3>
    <h4>Giant Leaps Will Always Come At A Cost.</h4>
    <fieldset>
      <input placeholder="Your name" type="text" tabindex="1" required autofocus/>
    </fieldset>
    <fieldset>
      <input placeholder="Your Email Address" type="email" tabindex="2" required/>
    </fieldset>
    <fieldset>
      <input placeholder="Your Phone Number (optional)" type="tel" tabindex="3" />
    </fieldset>
    <fieldset>
      <input placeholder="Your Web Site (optional)" type="url" tabindex="4" />
    </fieldset>
    <fieldset>
      <textarea placeholder="Type your message here...." tabindex="5" required></textarea>
    </fieldset>
    <fieldset>
      <button name="submit" type="submit" id="contact-submit" data-submit="...Sending">Submit</button>
    </fieldset>
  </form>

    </div>
    <div className="col-md-6 pt-lg-0  order-1 order-lg-2  contact-img">
          <img src={Web} className="img-fluid animated" alt="Venom" /> 
      
       </div> 
  </div>
  </div>
       </div>
     
       </div>
       </section>


            
        </>
    )
}

export default Contact
