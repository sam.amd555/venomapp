import React from 'react'
const Card = (props) => {

    return (
        <>
  <div className="col-md-4 col-10 mx-auto">
  <div className="card">
  <img src={props.imgsrc} className="card-img-top img-fluid" alt="Movie"/>
  <div className="card-body">
    <h3 className="card-title">{props.title}</h3>
    <p className="card-text">{props.para}</p>
    <div className="mt-4 b-prime">
    <a href={props.link} target='_blank' rel="noreferrer" className="btn-get-started">Watch Now</a>
       </div>
  </div>
</div>
</div>      
        </>
    )
}

export default Card
