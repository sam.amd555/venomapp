import React from 'react'

const Footer = () => {
    return (
        <>
            <footer className ="w-100 nav-bg text-center">
             <div className="footer-p">
             <a href="https://www.facebook.com/sam.amd36/" target="_blank" rel="noopener noreferrer"><i className="fab fa-facebook"></i></a>
             <a href="https://www.instagram.com/smzamd/" target="_blank" rel="noopener noreferrer"><i className="fab fa-instagram"></i></a>
             <a href="https://gitlab.com/sam.amd555" target="_blank" rel="noopener noreferrer"><i className="fab fa-gitlab"></i></a>
             <p>Developed & Desgin by Sameer Ahmed</p>
             </div>

            </footer>
        </>
    )
}

export default Footer
