import React from 'react'
import vid from '../images/one.mp4';
 const About = () => {
    return (
        <>
               <section className="video">
            
            <div className="container-fluid ">
            <div className="row">
              <div className="col-10 mx-auto">
              <video className="vid" src={vid} controls autoPlay></video>
              <div className="des">
              <h1 className="my-5 col-10">Description</h1>
           
            <p>Venom, or the Venom Symbiote, is a fictional extraterrestrial life
             form appearing in books published by Marvel Comics, specifically those featuring Spider-Man. 
             The creature is a sentient alien Symbiote, with a gooey, almost liquid-like form. It requires a host, 
             usually human, to bond with for its survival. In return the Venom creature gives its host enhanced powers.
              When the Venom Symbiote bonds with a human, that new dual-life form itself is also often called Venom.
               The Symbiote's first known host was Spider-Man, who eventually separated himself from the creature when 
               he discovered it was a life form attempting to permanently bond itself to him. The Symbiote went on to merge
                with other hosts and so began its reign as the villain known as Venom. Its second host, Eddie Brock, after bonding 
                with the Symbiote to become the first Venom, is one of Spider-Man's archenemies.Comics journalist and historian Mike
                 Conroy writes of the character: "What started out as a replacement costume for Spider-Man turned into one of the Marvel
                  web-slinger's greatest nightmares." Venom was ranked as the 22nd Greatest Comic Book Villain of All Time in IGN's list of
                   the top 100 comic villains, 33rd on Empire's 50 Greatest Comic Book Characters, and was ranked as the 98th Greatest Comic
                    Book Character Ever in Wizard magazine's 200 Greatest Comic Book Characters of all Time list.</p>
                    <p className="mt-5">
                TM & © Sony (2018)
                Fair use. 
                Copyright Disclaimer Under Section 107 of the Copyright Act 1976, allowance is made for "fair use" for purposes such as criticism, comment, news reporting, teaching, scholarship, and research. Fair use is a use permitted by copyright statute that might otherwise be infringing. Non-profit, 
                educational or personal use tips the balance in favor of fair use. No copyright infringement intended.
            </p>
              </div>
              </div>
             
            </div>
           
            </div>
           
            </section>
        </>
    )
}

export default About