import React from 'react'
import { NavLink } from 'react-router-dom';
import Web from "../images/venom.jpg"


 const Home = () => {
    return (
        <>
        <section id="header" className="d-flex align-item-center">
       <div className="container-fluid">
       <div className="row">
       <div className="col-10 mx-auto">
       <div className="row">
       <div className="col-md-6 mt-5 pt-lg-0 order-2 order-lg-1 d-flex justify-contain-center flex-column">
       <h1>We Are <strong className="brand">Venom.</strong></h1>
       <h3 mt-4>They Try And Silence Those Of Us Who Ask Questions. But You Know What? In The End, We're The Ones Who Change The World.</h3>
       <div className="mt-4 b-prime">
           <NavLink exact to="/Contact" className="btn-get-started">Join Us Now</NavLink>
       </div>
       </div>
       <div className="col-md-6 mt-5 pt-lg-0  order-1 order-lg-2 header-img">
          <img src={Web} className="img-fluid animated" alt="Venom" /> 
      
       </div> 
       </div>
    
       </div>

       </div>
       </div>
        </section>
        </>
    )
}

export default Home
