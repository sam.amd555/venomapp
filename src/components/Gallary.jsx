import React from 'react'
import { useState } from 'react'
import Gdata from './Gdata'

function Gallary(props) {
    const [item, setitem] = useState(Gdata)

    const filterItem =(cateItem)=>{
        const updateItem = Gdata.filter((currentElem)=>{
            return currentElem.category ===  cateItem;
        });
        setitem(updateItem);
    }
    return (
        <>
       <div className="container-fluid mx-auto">
       <div className="row">
       <div className="col-10 mx-auto">
       <div className="tag">
       <h2 className ="text-center my-4">Super Hero Wallpapers</h2>
       <hr />
       <div className="button">
       <button  name="Add" type="submit" onClick={()=> filterItem('venom')} >Venom</button>
       <button  name="Add" type="submit" onClick={()=> filterItem('spiderman')} > Spiderman </button>
       <button  name="Add" type="submit" onClick={()=> filterItem('other')}>Other</button>
       <button  name="Add" type="submit" onClick={()=> setitem(Gdata)} >All </button>
       </div>
       </div>
       <main className="grid my-20">
      
       {
           item.map((elem) =>{

               const {imaged} = elem;

               return(
                 
              
               <img src={imaged} alt="wallpaper"/>
              
            
               )
           })
           
           }
           </main>
           </div>
           </div>
       </div>
       </>
    )
}

export default Gallary
