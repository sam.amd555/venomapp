import React, { useState, useEffect } from 'react'
 const List = () => {
     const [inputData, setInputData] = useState([]);
     const [items, setItems] = useState([])

     
     const addItem = () =>{
    if(!inputData ) {
    }
    else{
      const allInputData = {id:new Date().getTime(), name:inputData}
      setItems ([...items, allInputData]);
      setInputData()
     }
    };

    //deleitem
    const deleteItem = (index) =>{
        const updateditems = items.filter((elem) =>{
        return !index === elem.id;
        })
        setItems(updateditems);
    }

    //remove all 
    const remove = ()=>{
        setItems([]);
    }

    useEffect(() => {
        localStorage.setItem("inputData", JSON.stringify(inputData));
      }, [inputData]);
    return (
        <>
      <div className="container-fluid">
      <div className="row">
          <div className="col-md-10 mx-auto text-center movieList">
            <div className="icon">
            <i className="fas fa-video"></i>
            <h3>Watch To list</h3>
            <div className="input-btn">
            <input type="text" placeholder="Enter Your favourite movie list" value={inputData} onChange={(e) =>setInputData(e.target.value) }/>
            <div className="add">
            <i className="fas fa-plus" title ="Add Item" onClick={addItem}></i></div>
            </div>
            
            <div className="showItems">
           
            {
                items.map((elem) =>{
               return(
                <div className="wrap" key={elem.id}>
                <div className="eachItems" >
                    <h4>{elem.name}</h4>
                    <h4><i className="fas fa-trash" title="Delete Items" onClick= {() => deleteItem(elem.id)}></i></h4>
                </div>
                <hr />
                 </div>     
                    )
                })
            }
            </div>
            <button  name="Add" type="submit" data-sm-link-text ="Remove All" onClick={remove}>Remove All Movie</button>
            </div>

          </div>
      </div>
      </div>


            
        </>
    )
  }
export default List
