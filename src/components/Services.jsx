import React from 'react'
import Card from './Card';
import Sdata from './Sdata';

 const Services = () => {
    return (
        <>
        <div className="my-5 text-center">
        <h1 className="">Watch Movies</h1>
        </div>
        <div className="container-fluid">
        <div className="row">
        <div className="col-10 mx-auto">
        <div className="row gy-5">
        {
      Sdata.map((val,ind) =>{
          return<Card key ={ind}
       id ={ind.id}
       imgsrc={val.imgsrc}
       title={val.title}
       para={val.para}
       link={val.link}
       />
      })
        }
        </div>
        </div>
        </div>
         </div>                 
                        
        </>
    );
}
export default Services
