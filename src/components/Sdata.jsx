import VENOM1 from '../images/venom1.jpg'
import Black from '../images/black.jpg'
import Logan from '../images/logan.jpg'
import Bat from '../images/bat.jpg'
import Galaxy from '../images/galaxy.jpg'
import Squad from '../images/squad.jpg'
import Money from '../images/money.jpg'
import Widow from '../images/widow.jpg'
import Aquaman from '../images/aquaman.jpg'
const Sdata = [
    {
    id: 1,
    imgsrc: VENOM1,
    title: "Venom",
    para: 'Ivestigative journalist Eddie Brock attempts a comeback following a scandal, but accidentally becomes the host of Venom, a violent, super powerful alien symbiote. Soon, he must rely on his newfound powers to protect the world from a shadowy organization looking for a symbiote of their own.',
    link : 'https://fmovies.app/watch-movie/watch-venom-online-19820.2507808'
    },
    {
        id: 2,
        imgsrc: Black,
        title: "Black Panther",
        para: "King T'Challa returns home from America to the reclusive, technologically advanced African nation of Wakanda to serve as his country's new leader. However, T'Challa soon finds that he is challenged for the throne by factions within his own country as well as without. Using powers reserved to Wakandan kings.",
        link : 'https://fmovies.app/movie/watch-black-panther-online-19797'
    },
    {
        id: 3,
        imgsrc: Logan,
        title: "Logan",
        para: "In the near future, a weary Logan cares for an ailing Professor X in a hideout on the Mexican border. But Logan's attempts to hide from the world and his legacy are upended when a young mutant arrives, pursued by dark forces.",
        link : 'https://fmovies.app/watch-movie/watch-venom-online-19820.2507808'
    },
    {
        id: 4,
        imgsrc: Bat,
        title: "The Dark Knight",
        para: "Batman raises the stakes in his war on crime. With the help of Lt. Jim Gordon and District Attorney Harvey Dent, Batman sets out to dismantle the remaining criminal organizations that plague the streets. The partnership proves to be effective, but they soon find themselves prey to a reign of chaos unleashed by a rising criminal mastermind known to the terrified citizens of Gotham as the Joker.",
        link : 'https://fmovies.app/watch-movie/watch-the-dark-knight-online-19752.2018414'
    },
    {
        id: 5,
        imgsrc: Galaxy,
        title: "Guardians of the Galaxy",
        para: 'After stealing a mysterious orb in the far reaches of outer space, Peter Quill from Earth is now the main target of a manhunt led by the villain known as Ronan the Accuser. To help fight Ronan and his team and save the galaxy from his power, Quill creates a team of space heroes known as the "Guardians of the Galaxy" to save the galaxy.',
        link : 'https://fmovies.app/watch-movie/watch-guardians-of-the-galaxy-online-19781.1613493'
    },
    {
        id: 6,
        imgsrc: Squad,
        title: "The Suicide Squad",
        para: "Supervillains Harley Quinn, Bloodsport, Peacemaker and a collection of nutty cons at Belle Reve prison join the super-secret, super-shady Task Force X as they are dropped off at the remote, enemy-infused island of Corto Maltese.",
        link : 'https://fmovies.app/watch-movie/watch-the-suicide-squad-online-7892.4563554'
    },
    {
        id: 7,
        imgsrc: Money,
        title: "Money Heist Part-5",
        para: "To carry out the biggest heist in history, a mysterious man called The Professor recruits a band of eight robbers who have a single characteristic: none of them has anything to lose. Five months of seclusion - memorizing every step, every detail, every probability - culminate in eleven days locked up in the National Coinage and Stamp Factory of Spain, surrounded by police forces and with dozens of hostages in their power, to find out whether their suicide wager will lead to everything or nothing.",
        link : 'https://fmovies.app/watch-tv/watch-money-heist-online-39409.1612143'
    },
    {
        id: 8,
        imgsrc: Widow,
        title: "Black Widow 2021",
        para: "Natasha Romanoff, also known as Black Widow, confronts the darker parts of her ledger when a dangerous conspiracy with ties to her past arises. Pursued by a force that will stop at nothing to bring her down, Natasha must deal with her history as a spy and the broken relationships left in her wake long before she became an Avenger.",
        link : "https://fmovies.app/watch-movie/watch-black-widow-online-66666.4534076"
    },
    {
        id: 9,
        imgsrc: Aquaman,
        title: "Aquaman",
        para: "Once home to the most advanced civilization on Earth, Atlantis is now an underwater kingdom ruled by the power-hungry King Orm. With a vast army at his disposal, Orm plans to conquer the remaining oceanic people and then the surface world. Standing in his way is Arthur Curry, Orm's half-human, half-Atlantean brother and true heir to the throne.",
        link : "https://fmovies.app/watch-movie/watch-aquaman-online-19837.1615747"
    },
];

export default Sdata
