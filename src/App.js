
import React from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/bootstrap/dist/js/bootstrap.bundle';
import './App.css';
import  Home  from './components/Home';
import  About  from './components/About';
import  Services  from './components/Services';
import  Contact  from './components/Contact';
import  Navbar  from './components/Navbar';
import Footer from './components/Footer';
import List from './components/List';
import Gallary from './components/Gallary';

import {Switch , Route, Redirect} from "react-router-dom";

function App() {
  return (
   <>
   <Navbar/>
   
<Switch>
   <Route exact path="/" component={Home}/>
  <Route exact path="/About" component={About}/>
  <Route exact path = '/Services' component = {Services} />
  <Route exact path = '/Contact' component = {Contact} />
  <Route exact path = '/List' component = {List} />
  <Route exact path = '/Gallary' component = {Gallary} />
  <Redirect to = '/' />
</Switch>
 
   <Footer/>
   </>
  );
}

export default App;
